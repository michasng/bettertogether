import 'package:better_together/models/model.dart';

class User implements Model {
  final String id;
  final String displayName;
  final String photoUrl;
  final List<dynamic> friends;

  User(
      {this.id,
      this.displayName = '',
      this.photoUrl = '',
      this.friends = const []});

  factory User.fromMap(String id, Map data) {
    return User(
      id: id,
      displayName: data['displayName'] ?? '',
      photoUrl: data['photoUrl'] ?? '',
      friends: data['friends'] ?? [],
    );
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['displayName'] = displayName;
    map['photoUrl'] = photoUrl;
    map['friends'] = friends;
    return map;
  }
}
