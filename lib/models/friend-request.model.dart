import 'package:better_together/models/model.dart';

class FriendRequest implements Model {
  final String id;
  final String fromUser, toUser;

  FriendRequest({this.id, this.fromUser, this.toUser});

  factory FriendRequest.fromMap(String id, Map data) {
    return FriendRequest(
      id: id,
      fromUser: data['fromUser'] ?? '',
      toUser: data['toUser'] ?? '',
    );
  }

  @override
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['fromUser'] = fromUser;
    map['toUser'] = toUser;
    return map;
  }
}
