import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

abstract class Service {
  final Firestore _db = Firestore.instance;
  Firestore get db => _db;
  final String _collection;
  String get collection => _collection;

  Service(String collection) : this._collection = collection;
}

T getProvider<T>(BuildContext context) {
  return Provider.of<T>(context, listen: false);
}
