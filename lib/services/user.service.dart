import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class UserService extends Service {
  UserService() : super('users');

  Future<User> getUser(String id) {
    return db
        .collection(collection)
        .document(id)
        .get()
        .then((snap) => User.fromMap(snap.documentID, snap.data));
  }

  Stream<User> streamUser(String id) {
    return db.collection(collection).document(id).snapshots().map((snap) {
      if (snap.data == null) return null;
      return User.fromMap(snap.documentID, snap.data);
    });
  }

  Stream<List<User>> streamUsers(
      {@required String searchTerm, List<String> idsToFilter}) {
    Query ref = db.collection(collection);

    if (searchTerm.length > 0)
      ref = ref.where('displayName', isEqualTo: searchTerm);

    return ref.snapshots().map((list) {
      var result = list.documents
          .map((doc) => User.fromMap(doc.documentID, doc.data))
          .toList();
      if (idsToFilter != null) {
        // reverse iteration to remove
        for (int i = result.length - 1; i >= 0; i--) {
          if (idsToFilter.contains(result[i].id)) result.removeAt(i);
        }
      }
      return result;
    });
  }

  Future<void> updateDisplayName(String id, String displayName) {
    return db
        .collection(collection)
        .document(id)
        .updateData({'displayName': displayName});
  }

  Future<void> updatePhotoUrl(String id, String photoUrl) {
    return db
        .collection(collection)
        .document(id)
        .updateData({'photoUrl': photoUrl});
  }

  Future<void> addFriend(
      {@required BuildContext context, @required String friendId}) async {
    var user = getProvider<User>(context);

    await db.collection(collection).document(user.id).updateData({
      'friends': FieldValue.arrayUnion([friendId])
    });
    return await db.collection(collection).document(friendId).updateData({
      'friends': FieldValue.arrayUnion([user.id])
    });
  }

  Future<void> removeFriend(
      {@required BuildContext context, @required String friendId}) async {
    var user = getProvider<User>(context);

    await db.collection(collection).document(user.id).updateData({
      'friends': FieldValue.arrayRemove([friendId])
    });
    return await db.collection(collection).document(friendId).updateData({
      'friends': FieldValue.arrayRemove([user.id])
    });
  }

  Future<String> createUser(
      String displayName, String email, String password) async {
    var authResult = await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    // await authResult.user.sendEmailVerification();
    try {
      var id = authResult.user.uid;
      var user = User(
        id: id,
        displayName: displayName,
        photoUrl: '',
        friends: [],
      );
      // sets the id, instead of generating a new one, like collection.add()
      await db.collection(collection).document(id).setData(user.toMap());
      print('created user ' + authResult.user.toString());
      return id;
    } catch (e) {
      authResult.user.delete();
      throw e;
    }
  }

  Future<void> deleteUser(String password) async {
    var auth = FirebaseAuth.instance;
    var user = await auth.currentUser();
    await auth.signInWithEmailAndPassword(
      email: user.email,
      password: password,
    );
    await db.collection(collection).document(user.uid).delete();
    return user.delete();
  }
}
