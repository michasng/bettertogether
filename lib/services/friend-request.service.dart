import 'package:better_together/models/friend-request.model.dart';
import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FriendRequestService extends Service {
  FriendRequestService() : super('friend_requests');

  Stream<List<FriendRequest>> streamFriendRequests(
      {@required BuildContext context, @required bool received}) {
    var user = getProvider<User>(context);
    var query = db
        .collection(collection)
        .where(received ? 'toUser' : 'fromUser', isEqualTo: user.id);

    return _mapQuery(query);
  }

  Stream<FriendRequest> streamFriendRequest(
      {@required BuildContext context,
      @required String fromUser,
      @required String toUser}) {
    var query = db
        .collection(collection)
        .where('fromUser', isEqualTo: fromUser)
        .where('toUser', isEqualTo: toUser);
    return _mapQuery(query).map((friendRequests) =>
        friendRequests.isNotEmpty ? friendRequests[0] : null);
  }

  Stream<List<FriendRequest>> _mapQuery(Query query) {
    return query.snapshots().map((list) => list.documents
        .map((doc) => FriendRequest.fromMap(doc.documentID, doc.data))
        .toList());
  }

  Future<void> sendFriendRequest(
      {@required BuildContext context, @required String targetUserId}) {
    var user = getProvider<User>(context);
    var friendRequest = FriendRequest(fromUser: user.id, toUser: targetUserId);
    return db.collection(collection).add(friendRequest.toMap());
  }

  Future<void> acceptFriendRequest(
      {@required BuildContext context,
      @required FriendRequest friendRequest}) async {
    var userService = Provider.of<UserService>(context, listen: false);

    await userService.addFriend(
        context: context, friendId: friendRequest.fromUser);
    return db.collection(collection).document(friendRequest.id).delete();
  }

  Future<void> denyFriendRequest({@required FriendRequest friendRequest}) {
    return db.collection(collection).document(friendRequest.id).delete();
  }

  // currently the same as denyFriendRequest, but impl. could change
  Future<void> cancelFriendRequest({@required FriendRequest friendRequest}) {
    return db.collection(collection).document(friendRequest.id).delete();
  }
}
