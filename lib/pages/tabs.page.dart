import 'package:better_together/pages/settings.page.dart';
import 'package:better_together/pages/tabs/explore.tab.dart';
import 'package:better_together/pages/tabs/friends.tab.dart';
import 'package:better_together/pages/tabs/posting.tab.dart';
import 'package:flutter/material.dart';

class TabsPage extends StatefulWidget {
  final int initialTabIndex;

  final List<Widget> _tabs = [
    ExploreTab(),
    PostingTab(),
    FriendsTab(),
  ];

  TabsPage({Key key, this.initialTabIndex = 1}) : super(key: key);

  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  int _tabIndex;

  @override
  void initState() {
    _tabIndex = widget.initialTabIndex;
    super.initState();
  }

  void _selectTab(int tabIndex) {
    setState(() {
      _tabIndex = tabIndex;
    });
  }

  void _navigateToSettings() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SettingsPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Better Together'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: _navigateToSettings,
          ),
        ],
      ),
      body: widget._tabs.elementAt(_tabIndex),
      bottomNavigationBar: BottomNavigationBar(
        iconSize: 32,
        type: BottomNavigationBarType.shifting,
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.amber,
        items: [
          BottomNavigationBarItem(
            title: Text('Explore'),
            icon: Icon(Icons.map),
          ),
          BottomNavigationBarItem(
            title: Text('New Post'),
            icon: Icon(Icons.add_circle),
          ),
          BottomNavigationBarItem(
            title: Text('Friends'),
            icon: Icon(Icons.people),
          ),
        ],
        currentIndex: _tabIndex,
        onTap: _selectTab,
      ),
    );
  }
}
