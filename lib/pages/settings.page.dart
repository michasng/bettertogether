import 'package:better_together/widgets/forms/display-name-edit.widget.dart';
import 'package:better_together/widgets/forms/email-edit.widget.dart';
import 'package:better_together/widgets/forms/password-edit.widget.dart';
import 'package:better_together/widgets/forms/phone-number-edit.widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  void _signOut() async {
    var auth = FirebaseAuth.instance;
    await auth.signOut();
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: <Widget>[
            Card(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  DisplayNameEditWidget(),
                  Divider(),
                  EmailEditWidget(),
                  Divider(),
                  PhoneNumberEditWidget(),
                  Divider(),
                  PasswordEditWidget(),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: FlatButton.icon(
                icon: Icon(Icons.power_settings_new),
                label: Text('Sign Out'),
                onPressed: _signOut,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
