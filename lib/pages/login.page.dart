import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email, _password;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();

  Future<void> _signIn() async {
    final formState = _formKey.currentState;
    if (!formState.validate()) return;
    formState.save();

    var auth = FirebaseAuth.instance;
    try {
      await auth.signInWithEmailAndPassword(email: _email, password: _password);
      Navigator.pop(context);
    } catch (e) {
      var ex = (e as PlatformException);
      print('sign in failed: ${ex.code}, ${ex.message}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              TextFormField(
                focusNode: _emailFocus,
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (value) {
                  _emailFocus.unfocus();
                  FocusScope.of(context).requestFocus(_passwordFocus);
                },
                validator: (input) {
                  if (input.isEmpty) return 'Please enter email';
                  return null;
                },
                onSaved: (input) => _email = input,
                decoration: InputDecoration(
                  labelText: 'Email',
                ),
              ),
              TextFormField(
                focusNode: _passwordFocus,
                onFieldSubmitted: (value) => _signIn(),
                validator: (input) {
                  if (input.isEmpty) return 'Please enter password';
                  return null;
                },
                onSaved: (input) => _password = input,
                decoration: InputDecoration(
                  labelText: 'Password',
                ),
                obscureText: true,
              ),
              RaisedButton(
                onPressed: _signIn,
                child: Text('Sing In'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
