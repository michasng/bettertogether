import 'package:async/async.dart';
import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/friend-request.service.dart';
import 'package:better_together/services/service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:better_together/widgets/search-users.dialog.dart';
import 'package:better_together/widgets/user-tile.widget.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class FriendsTab extends StatefulWidget {
  @override
  _FriendsTabState createState() => _FriendsTabState();
}

class _FriendsTabState extends State<FriendsTab> {
  final List<UsersExpansionPanel> expansionPanels = [];

  @override
  void initState() {
    super.initState();
    expansionPanels.add(UsersExpansionPanel(
        isExpanded: true, sectionName: 'Received Friend Requests'));
    expansionPanels
        .add(UsersExpansionPanel(isExpanded: true, sectionName: 'Friends'));
    expansionPanels.add(UsersExpansionPanel(
        isExpanded: true, sectionName: 'Sent Friend Requests'));
  }

  void _populateExpansionPanels() {
    var friendRequestService = getProvider<FriendRequestService>(context);
    var userService = getProvider<UserService>(context);
    expansionPanels[0].stream = friendRequestService
        .streamFriendRequests(context: context, received: true)
        .transform(
          FlatMapStreamTransformer((friendRequests) => StreamZip(friendRequests
              .map<Stream<User>>((fr) => userService.streamUser(fr.fromUser)))),
        );

    var user = getProvider<User>(context);
    expansionPanels[1].stream = Stream.value(user.friends).transform(
      FlatMapStreamTransformer((friends) => StreamZip(friends
          .map<Stream<User>>((friend) => userService.streamUser(friend)))),
    );
    expansionPanels[2].stream = friendRequestService
        .streamFriendRequests(context: context, received: false)
        .transform(
          FlatMapStreamTransformer((friendRequests) => StreamZip(friendRequests
              .map<Stream<User>>((fr) => userService.streamUser(fr.toUser)))),
        );
  }

  Future<void> _openPeopleSearch() {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) => SearchUsersDialog());
  }

  @override
  Widget build(BuildContext context) {
    _populateExpansionPanels();

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.amber,
        onPressed: _openPeopleSearch,
      ),
      body: SingleChildScrollView(
        child: Container(
          child: ExpansionPanelList(
            expansionCallback: (int index, bool isExpanded) {
              setState(() {
                expansionPanels[index].isExpanded = !isExpanded;
              });
            },
            children:
                expansionPanels.map((panel) => panel.build(context)).toList(),
          ),
        ),
      ),
    );
  }
}

class UsersExpansionPanel {
  bool isExpanded;
  String sectionName;
  Stream<List<User>> stream;

  UsersExpansionPanel(
      {this.sectionName, this.stream, @required this.isExpanded});

  ExpansionPanel build(BuildContext context) {
    return ExpansionPanel(
      canTapOnHeader: true,
      headerBuilder: (BuildContext context, bool isExpanded) {
        return ListTile(
          title: Text(sectionName),
        );
      },
      body: StreamBuilder<List<User>>(
        stream: stream,
        builder: (context, snapshot) {
          if (snapshot.connectionState != ConnectionState.active)
            return Container();
          var users = snapshot.data;
          return Column(
            children: users.map((User user) => UserTileWidget(user)).toList(),
          );
        },
      ),
      isExpanded: isExpanded,
    );
  }
}
