import 'package:better_together/services/user.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _displayNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _confirmPasswordFocus = FocusNode();
  final TextEditingController _passwordCtrl = TextEditingController();
  bool _formEnabled = true;
  String _displayName, _email, _password;

  Future<void> _signUp() async {
    final formState = _formKey.currentState;
    if (!formState.validate()) return;
    formState.save();

    var userService = Provider.of<UserService>(context, listen: false);

    setState(() => _formEnabled = false);
    try {
      await userService.createUser(_displayName, _email, _password);
      // ToDo: visualize that the user should verify his email
      Navigator.pop(context);
    } catch (e) {
      var ex = (e as PlatformException);
      print('sign up failed: ${ex.code}, ${ex.message}');
    }
    setState(() => _formEnabled = true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              _buildDisplayNameField(),
              _buildEmailField(),
              _buildPasswordField(),
              _buildConfirmPasswordField(),
              FlatButton(
                onPressed: _formEnabled ? _signUp : null,
                child: _formEnabled
                    ? Text('Sing Up')
                    : SpinKitSpinningCircle(
                        size: 24,
                        color: Colors.grey,
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _shiftFocus(oldFocus, newFocus) {
    oldFocus.unfocus();
    FocusScope.of(context).requestFocus(newFocus);
  }

  Widget _buildDisplayNameField() {
    return TextFormField(
      enabled: _formEnabled,
      focusNode: _displayNameFocus,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) => _shiftFocus(_displayNameFocus, _emailFocus),
      validator: (input) {
        if (input.isEmpty && input.length < 16)
          return 'Please enter display name';
        return null;
      },
      onSaved: (input) => _displayName = input,
      decoration: InputDecoration(
        labelText: 'Display Name',
      ),
    );
  }

  Widget _buildEmailField() {
    return TextFormField(
      enabled: _formEnabled,
      focusNode: _emailFocus,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) => _shiftFocus(_emailFocus, _passwordFocus),
      validator: (input) {
        if (input.isEmpty) return 'Please enter email';
        return null;
      },
      onSaved: (input) => _email = input,
      decoration: InputDecoration(
        labelText: 'Email',
      ),
    );
  }

  Widget _buildPasswordField() {
    return TextFormField(
      enabled: _formEnabled,
      controller: _passwordCtrl,
      focusNode: _passwordFocus,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) =>
          _shiftFocus(_passwordFocus, _confirmPasswordFocus),
      validator: (input) {
        if (input.length < 6)
          return 'Your password needs to have at least 6 characters';
        return null;
      },
      onSaved: (input) => _password = input,
      decoration: InputDecoration(
        labelText: 'Password',
      ),
      obscureText: true,
    );
  }

  Widget _buildConfirmPasswordField() {
    return TextFormField(
      enabled: _formEnabled,
      focusNode: _confirmPasswordFocus,
      onFieldSubmitted: (value) => _signUp(),
      validator: (input) {
        if (input != _passwordCtrl.text) return 'Your passwords don\'t match';
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Confirm Password',
      ),
      obscureText: true,
    );
  }
}
