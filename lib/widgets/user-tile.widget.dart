import 'package:better_together/models/friend-request.model.dart';
import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/friend-request.service.dart';
import 'package:better_together/services/service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UserTileWidget extends StatefulWidget {
  final User user;

  const UserTileWidget(this.user, {Key key}) : super(key: key);

  @override
  _UserTileWidgetState createState() => _UserTileWidgetState();
}

class _UserTileWidgetState extends State<UserTileWidget> {
  FriendRequest _friendRequest;

  Future<void> _onAddPressed(bool sent, bool received) async {
    var friendRequestService = getProvider<FriendRequestService>(context);
    String action;
    try {
      if (!sent && !received) {
        action = 'sending friend request';
        return await friendRequestService.sendFriendRequest(
            context: context, targetUserId: widget.user.id);
      }
      if (received && !sent) {
        action = 'accepting friend request';
        return await friendRequestService.acceptFriendRequest(
            context: context, friendRequest: _friendRequest);
      }
    } catch (e) {
      var ex = (e as PlatformException);
      print('$action failed: ${ex.code}, ${ex.message}');
    }
  }

  Future<void> _onRemovePressed(bool sent, bool received) async {
    var friendRequestService = getProvider<FriendRequestService>(context);
    String action;
    try {
      if (received && !sent) {
        action = 'denying friend request';
        return await friendRequestService.denyFriendRequest(
            friendRequest: _friendRequest);
      }
      if (sent && !received) {
        action = 'canceling friend request';
        return await friendRequestService.cancelFriendRequest(
            friendRequest: _friendRequest);
      }
      if (!sent && !received) {
        var userService = getProvider<UserService>(context);
        action = 'removing friend';
        return await userService.removeFriend(
            context: context, friendId: widget.user.id);
      }
    } catch (e) {
      var ex = (e as PlatformException);
      print('$action failed: ${ex.code}, ${ex.message}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(widget.user.displayName),
      subtitle: Text('420'),
      leading: CircleAvatar(
        radius: 26,
        backgroundImage: NetworkImage(widget.user.photoUrl),
      ),
      onTap: () {
        // ToDo: visit profile
      },
      trailing: _buildButtons(),
    );
  }

  Widget _buildButtons() {
    var friendRequestService = getProvider<FriendRequestService>(context);
    var user = getProvider<User>(context);
    return StreamBuilder<FriendRequest>(
        stream: friendRequestService.streamFriendRequest(
            context: context, fromUser: user.id, toUser: widget.user.id),
        builder: (context, sentSnapshot) {
          if (sentSnapshot.connectionState != ConnectionState.active)
            return _buildLoadingIcon();
          return StreamBuilder<FriendRequest>(
              stream: friendRequestService.streamFriendRequest(
                  context: context, fromUser: widget.user.id, toUser: user.id),
              builder: (context, receivedSnapshot) {
                if (receivedSnapshot.connectionState != ConnectionState.active)
                  return _buildLoadingIcon();

                bool sent = sentSnapshot.hasData;
                bool received = receivedSnapshot.hasData;
                bool friends = user.friends.contains(widget.user.id);

                _friendRequest = sent
                    ? sentSnapshot.data
                    : received ? receivedSnapshot.data : null;

                return Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // same as: (!sent && !received) || (!sent && received)
                    if (!friends && !sent)
                      IconButton(
                        icon: Icon(
                          Icons.add_circle_outline,
                          color: Colors.green,
                        ),
                        onPressed: () => _onAddPressed(sent, received),
                      ),
                    if ((received && !sent) || (sent && !received) || friends)
                      IconButton(
                        icon: Icon(
                          Icons.remove_circle_outline,
                          color: Colors.red,
                        ),
                        onPressed: () => _onRemovePressed(sent, received),
                      ),
                  ],
                );
              });
        });
  }

  Widget _buildLoadingIcon() {
    return Icon(
      Icons.wifi,
    );
  }
}
