import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

typedef Future<bool> BoolFuture();

class TextEditWidget extends StatefulWidget {
  final String variableName;
  final String originalValue;

  /// Returns any error messages, or null if successful.
  final Future<void> Function(String) onSave;

  /// Whether the user needs to login again to change this value.
  final bool requiresPassword;

  /// Whether the user needs to confirm the input by repeating it.
  final bool requiresConfirm;

  final bool obscureText;

  TextEditWidget({
    Key key,
    @required this.variableName,
    @required this.originalValue,
    @required this.onSave,
    this.requiresPassword = false,
    this.requiresConfirm = false,
    this.obscureText = false,
  }) : super(key: key);

  _TextEditWidgetState createState() => _TextEditWidgetState();
}

class _TextEditWidgetState extends State<TextEditWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final FocusNode _passwordFieldFocus = FocusNode();
  final FocusNode _formFieldFocus = FocusNode();
  final FocusNode _confirmFieldFocus = FocusNode();
  final TextEditingController _formFieldCtrl = TextEditingController();
  bool _isEditing = false;
  bool _formEnabled = true;
  String _password, _value;

  void _trySave() async {
    final formState = _formKey.currentState;
    if (!formState.validate()) return;
    formState.save();

    if (_value == widget.originalValue) {
      setState(() => _isEditing = false);
      return;
    }

    setState(() => _formEnabled = false);
    try {
      if (widget.requiresPassword) await reAuthenticate();
      await widget.onSave(_value);
      setState(() => _isEditing = false);
    } catch (e) {
      var ex = e as PlatformException;
      print(
          'updating ${widget.variableName} failed: ${ex.code}, ${ex.message}');
      // ToDo: display error messages
    }
    setState(() => _formEnabled = true);
  }

  /// Throws PlatformException.
  Future<void> reAuthenticate() async {
    var user = await FirebaseAuth.instance.currentUser();
    await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: user.email, password: _password);
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: _buildContent(),
    );
  }

  Widget _buildContent() {
    if (!_isEditing) return _buildAsLabel();

    return _buildAsForm();
  }

  Widget _buildAsLabel() {
    return ListTile(
      title: Text(widget.variableName),
      subtitle: Text(widget.originalValue),
      trailing: IconButton(
        icon: Icon(Icons.edit),
        onPressed: () => setState(() => _isEditing = true),
      ),
    );
  }

  Widget _buildAsForm() {
    List<Widget> formFields = [];
    if (widget.requiresPassword) formFields.add(_buildPasswordField());
    formFields.add(_buildFormField());
    if (widget.requiresConfirm) formFields.add(_buildConfirmField());

    return ListTile(
      title: ListView(
        shrinkWrap: true,
        children: formFields,
      ),
      trailing: IconButton(
        icon: _formEnabled
            ? Icon(Icons.check)
            : SpinKitSpinningCircle(
                size: 24,
                color: Colors.grey,
              ),
        onPressed: _formEnabled ? _trySave : null,
      ),
    );
  }

  Widget _buildPasswordField() {
    return TextFormField(
      enabled: _formEnabled,
      focusNode: _passwordFieldFocus,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (value) {
        _passwordFieldFocus.unfocus();
        FocusScope.of(context).requestFocus(_formFieldFocus);
      },
      validator: (input) {
        if (input.isEmpty) return 'Please enter your current password';
        return null;
      },
      onSaved: (input) => _password = input,
      decoration: InputDecoration(
        labelText: 'Current Password',
      ),
      obscureText: true,
    );
  }

  @override
  void setState(void Function() fn) {
    // workaround to have both a controller and an initialValue in the formfield
    _formFieldCtrl.text = widget.originalValue;
    super.setState(fn);
  }

  Widget _buildFormField() {
    return TextFormField(
      enabled: _formEnabled,
      controller: _formFieldCtrl,
      focusNode: _formFieldFocus,
      textInputAction:
          widget.requiresConfirm ? TextInputAction.next : TextInputAction.done,
      onFieldSubmitted: (value) {
        if (widget.requiresConfirm) {
          _formFieldFocus.unfocus();
          FocusScope.of(context).requestFocus(_confirmFieldFocus);
        } else
          _trySave();
      },
      validator: (input) {
        if (input.isEmpty)
          return 'Please enter ' + widget.variableName.toLowerCase();
        return null;
      },
      onSaved: (input) => _value = input,
      decoration: InputDecoration(
        labelText: widget.variableName,
      ),
      obscureText: widget.obscureText,
    );
  }

  Widget _buildConfirmField() {
    return TextFormField(
      enabled: _formEnabled,
      focusNode: _confirmFieldFocus,
      onFieldSubmitted: (value) => _trySave(),
      validator: (input) {
        if (input != _formFieldCtrl.text)
          return 'Your ${widget.variableName.toLowerCase()}s don\'t match';
        return null;
      },
      decoration: InputDecoration(
        labelText: 'Confirm ' + widget.variableName,
      ),
      obscureText: widget.obscureText,
    );
  }
}
