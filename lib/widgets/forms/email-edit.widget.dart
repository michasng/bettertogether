import 'package:better_together/widgets/forms/text-edit.widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class EmailEditWidget extends StatefulWidget {
  @override
  _EmailEditWidgetState createState() => _EmailEditWidgetState();
}

class _EmailEditWidgetState extends State<EmailEditWidget> {
  /// Throws PlatformException.
  Future<void> _onSave(String value) async {
    var user = await FirebaseAuth.instance.currentUser();
    await user.updateEmail(value);
    setState(() => {});
    // ToDo: visualize that the user should verify his email
    await user.sendEmailVerification();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<FirebaseUser>(
      future: FirebaseAuth.instance.currentUser(),
      builder: (context, snapshot) {
        var originalValue = '';
        if (snapshot.connectionState == ConnectionState.done)
          originalValue = snapshot.data.email;
        return TextEditWidget(
          variableName: 'Email',
          originalValue: originalValue,
          onSave: _onSave,
          requiresPassword: true,
        );
      },
    );
  }
}
