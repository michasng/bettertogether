import 'package:better_together/widgets/forms/text-edit.widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PasswordEditWidget extends StatefulWidget {
  @override
  _PasswordEditWidgetState createState() => _PasswordEditWidgetState();
}

class _PasswordEditWidgetState extends State<PasswordEditWidget> {
  /// Throws PlatformException.
  Future<void> _onSave(String value) async {
    var user = await FirebaseAuth.instance.currentUser();
    await user.updatePassword(value);
  }

  @override
  Widget build(BuildContext context) {
    return TextEditWidget(
      variableName: 'Password',
      originalValue: '',
      onSave: _onSave,
      requiresPassword: true,
      requiresConfirm: true,
      obscureText: true,
    );
  }
}
