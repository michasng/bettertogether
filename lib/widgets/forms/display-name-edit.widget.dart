import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:better_together/widgets/forms/text-edit.widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DisplayNameEditWidget extends StatefulWidget {
  @override
  _DisplayNameEditWidgetState createState() => _DisplayNameEditWidgetState();
}

class _DisplayNameEditWidgetState extends State<DisplayNameEditWidget> {
  Future<void> _onSave(String value) async {
    var userService = Provider.of<UserService>(context, listen: false);
    var user = getProvider<User>(context);
    await userService.updateDisplayName(user.id, value);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<User>(
      builder: (context, user, child) => TextEditWidget(
        variableName: 'Display Name',
        originalValue: (user != null) ? user.displayName : '',
        onSave: _onSave,
      ),
    );
  }
}
