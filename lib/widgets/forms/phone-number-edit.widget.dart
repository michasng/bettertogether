import 'package:better_together/widgets/forms/text-edit.widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class PhoneNumberEditWidget extends StatefulWidget {
  @override
  _PhoneNumberEditWidgetState createState() => _PhoneNumberEditWidgetState();
}

class _PhoneNumberEditWidgetState extends State<PhoneNumberEditWidget> {
  Future<void> _onSave(String value) async {
    var user = await FirebaseAuth.instance.currentUser();
    // ToDo: implement using reference: https://medium.com/@fayazfz07/firebase-phone-auth-with-flutter-db7e934ef46f
    await FirebaseAuth.instance.verifyPhoneNumber(
      codeAutoRetrievalTimeout: (String verificationId) {},
      codeSent: (String verificationId, [int forceResendingToken]) {},
      phoneNumber: value,
      timeout: Duration(seconds: 60),
      verificationCompleted: (AuthCredential phoneAuthCredential) {
        user.updatePhoneNumberCredential(phoneAuthCredential);
      },
      verificationFailed: (AuthException error) {},
    );
    setState(() => {});
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<FirebaseUser>(
      future: FirebaseAuth.instance.currentUser(),
      builder: (context, snapshot) {
        var originalValue = '';
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data.phoneNumber != null)
          originalValue = snapshot.data.phoneNumber;
        return TextEditWidget(
          variableName: 'Phone Number',
          originalValue: originalValue,
          onSave: _onSave,
        );
      },
    );
  }
}
