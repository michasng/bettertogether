import 'package:better_together/models/user.model.dart';
import 'package:better_together/services/service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:better_together/widgets/user-tile.widget.dart';
import 'package:flutter/material.dart';

class SearchUsersDialog extends StatefulWidget {
  SearchUsersDialog({Key key}) : super(key: key);

  _SearchUsersDialogState createState() => _SearchUsersDialogState();
}

class _SearchUsersDialogState extends State<SearchUsersDialog> {
  TextEditingController _searchCtrl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var searchBar = ListTile(
      title: TextField(
        controller: _searchCtrl,
      ),
      trailing: IconButton(
        icon: Icon(
          Icons.search,
          color: Colors.grey,
        ),
        onPressed: () {},
      ),
    );

    var userService = getProvider<UserService>(context);
    var user = getProvider<User>(context);
    return StreamBuilder<List<User>>(
      stream: userService
          .streamUsers(searchTerm: _searchCtrl.text, idsToFilter: [user.id]),
      builder: (context, snapshot) => SimpleDialog(
        titlePadding: EdgeInsets.zero,
        title: Container(
          color: Colors.amber,
          child: ListTile(
            // contentPadding: EdgeInsets.zero,
            // backgroundColor: Colors.amber,
            title: Text(
              'Search Friends',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.close, color: Colors.black),
              onPressed: () => Navigator.pop(context),
            ),
          ),
        ),
        children: [
          searchBar,
          ...snapshot.hasData
              ? snapshot.data.map((user) => UserTileWidget(user)).toList()
              : [],
        ],
      ),
    );
  }
}
