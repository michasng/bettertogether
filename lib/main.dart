import 'package:better_together/models/user.model.dart';
import 'package:better_together/pages/tabs.page.dart';
import 'package:better_together/pages/welcome.page.dart';
import 'package:better_together/services/friend-request.service.dart';
import 'package:better_together/services/user.service.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final String appName = 'Better Together';

  @override
  Widget build(BuildContext context) {
    var auth = FirebaseAuth.instance;
    UserService userService;
    return MultiProvider(
      providers: [
        Provider<UserService>.value(
          value: userService = new UserService(),
        ),
        Provider<FriendRequestService>.value(
          value: new FriendRequestService(),
        ),
        StreamProvider<User>.value(
          value: auth.onAuthStateChanged.transform(
            FlatMapStreamTransformer<FirebaseUser, User>(
              (firebaseUser) {
                if (firebaseUser == null) return Stream.value(null);
                var user = userService.streamUser(firebaseUser.uid);
                if (user == null) {
                  var ex = PlatformException(
                      code: 'DATABASE_DESYNC',
                      message: 'User does not exist in database.');
                  print('getting user failed: ${ex.code}, ${ex.message}');
                  auth.signOut();
                }
                return user;
              },
            ),
          ),
        ),
      ],
      child: MaterialApp(
        title: appName,
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        home: StreamBuilder<FirebaseUser>(
            stream: auth.onAuthStateChanged,
            builder: (context, snapshot) {
              if (!snapshot.hasData) return WelcomePage();
              return TabsPage();
            }),
      ),
    );
  }
}
