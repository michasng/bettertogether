# Better Together

A proof-of-concept social network App using Flutter and Firebase. This is a test project for registering and authentificating users and managing friends-lists in real-time using streams.

## Getting Started

Run the flutter project using `flutter run`. It might be advantageous to run multiple instances, to see how firebase automatically updates all local databases.

## Authors

Micha Sengotta
micha.sengotta@gmail.com

